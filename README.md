
Jamf Pro Serial Number Importer for macOS 10.13+
====================================================

Use this repo to update your device's Serial Number with Workplace Technology by following the instructions below. This should take no more than 5 minutes.

How to update your Serial Number
--------------------------------

1. Clone this repo
2. `make`
3. `system_profiler SPHardwareDataType | awk '/Serial/ {print $4}')`
4. If your machine's hostname is a serial number, check that it matches the serial number printed by the above command
5. If it matches, or your machine's hostname is not a serial number, you are done
6. If it does not match, run `sudo scutil --set HostName <serial number from step 3>`


Troubleshooting
---------------

#### `command not found: make`
Run `brew install make --with-default-names`, and run `make` again.

### Docker DNS no longer resolves 
Reinstall docker via `brew cask` as in [this StackOverflow answer](https://stackoverflow.com/questions/44084846/cannot-connect-to-the-docker-daemon-on-macos)


About JSSImporter
----------------------

This processor adds the ability for [AutoPkg](https://github.com/autopkg/autopkg) to create groups, upload packages, manage serial numbers, add scripts and extension attributes, and create policies for the Jamf Pro Server, allowing you to fully-automate your software **testing** workflow.

Acknowledgements
----------------

Huge thanks to Shea Craig, who wrote the bulk of this work and is still providing advice, though not currently involved in Jamf administration.

License
-------

See the LICENSE file.
