CURDIR := $(shell pwd)
PKG_ROOT := $(CURDIR)/pkg/jssimporter/payload
PKG_BUILD := $(CURDIR)/pkg/jssimporter/build
PKG_VERSION := $(shell defaults read $(CURDIR)/pkg/jssimporter/build-info.plist version)
JSS_GIT_ROOT := $(abspath $(CURDIR)/../python-jss)
PIP := $(shell hash pip3 2>/dev/null && echo "pip3" || echo "pip")
  

objects = "$(PKG_ROOT)/Library/Application Support/JSSImporter/requests" \
	"$(PKG_ROOT)/Library/Application Support/JSSImporter/boto" \
	"$(PKG_ROOT)/Library/Application Support/JSSImporter/jss" \
	$(PKG_ROOT)/Library/AutoPkg/autopkglib/JSSImporter.py 


default : $(PKG_BUILD)/jssimporter-$(PKG_VERSION).pkg
	@echo "Using python-jss git source from $(JSS_GIT_ROOT)"


$(PKG_BUILD)/jssimporter-$(PKG_VERSION).pkg: $(objects)
	@echo "JSS system_profiler install successful"


"$(PKG_ROOT)/Library/Application Support/JSSImporter/boto":
	@echo "Installing boto into JSSImporter support directory"
	#$(PIP) install --install-option="--prefix=$(PKG_ROOT)/Library/Application Support/JSSImporter/boto" --ignore-installed boto
	$(PIP) install --target "$(PKG_ROOT)/Library/Application Support/JSSImporter" --ignore-installed boto


"$(PKG_ROOT)/Library/Application Support/JSSImporter/requests":
	@echo "Installing requests into JSSImporter support directory"
	#$(PIP) install --install-option="--prefix=$(PKG_ROOT)/Library/Application Support/JSSImporter/requests" --ignore-installed requests
	$(PIP) install --target "$(PKG_ROOT)/Library/Application Support/JSSImporter" --ignore-installed requests


"$(PKG_ROOT)/Library/Application Support/JSSImporter/jss":
	@echo "Installing python-jss"
	mkdir -p "$(PKG_ROOT)/Library/Application Support/JSSImporter"
	#@echo "Using amended PYTHONPATH inside package root, otherwise easy_install will complain we arent installing to a PYTHONPATH"
	#cd $(JSS_GIT_ROOT) && PYTHONPATH="$(PKG_ROOT)/Library/Application Support/JSSImporter" easy_install --install-dir "$(PKG_ROOT)/Library/Application Support/JSSImporter" .

$(PKG_ROOT)/Library/AutoPkg/autopkglib/JSSImporter.py:
	@echo "Copying JSSImporter.py into autopkglib"
	mkdir -p "$(PKG_ROOT)/Library/AutoPkg/autopkglib"
	cp $(CURDIR)/JSSImporter.py $(PKG_ROOT)/Library/AutoPkg/autopkglib/JSSImporter.py
	chmod 755 "$(PKG_ROOT)/Library/AutoPkg/autopkglib/JSSImporter.py"
	@git clone https://github.com/jamfsoftware/system_profiler
	$(PIP) install --target "$(PKG_ROOT)/Library/Application Support/JSSImporter" --ignore-installed system_profiler/

.PHONY : clean
clean :
	@echo "Cleaning up package root"
	rm $(PKG_ROOT)/Library/AutoPkg/autopkglib/JSSImporter.py
	rm -rf "$(PKG_ROOT)/Library/Application Support/JSSImporter/*"
	rm $(CURDIR)/pkg/jssimporter/build/*.pkg
